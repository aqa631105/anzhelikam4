Feature: Test crud for https://petstore.swagger.io/v2

  Scenario: Create a new base user
    When I create a new user
    Then Status code is 200

  Scenario: Get the user created before
    And User with required data exists
    When I get the user
    Then Status code is 200
    And Response contains user's username

  Scenario Outline: Update the user created before
    And User with required data exists
    When I update the user with '<email>'
    Then Status code is 200
    Examples:
      | email           |
      | test@email.com  |
      | test2@email.com |

  Scenario: Delete user
    And User with required data exists
    When I delete the user
    Then Status code is 200
    And Impossible to get deleted user