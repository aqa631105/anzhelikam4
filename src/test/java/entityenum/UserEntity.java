package entityenum;


import static utils.EmailGenerator.emailGenerator;

/**
 * Enum representing various fields related to a user entity.
 */
public enum UserEntity {

    USERNAME("name"),
    UPDATED_USERNAME("updatedName"),
    FIRST_NAME("Andrew"),
    LAST_NAME("Green"),
    EMAIL(emailGenerator()),
    PASSWORD("123qwe"),
    PHONE("11111111"),
    USERID(1),
    USERSTATUS(1);

    private final String fieldName;
    private final int fieldInt;


    UserEntity(String fieldName) {
        this.fieldName = fieldName;
        fieldInt = 0;
    }

    UserEntity(int fieldInt) {
        this.fieldInt = fieldInt;
        fieldName = null;
    }

    public String getFieldName() {
        return fieldName;
    }

    public int getFieldInt() {
        return fieldInt;
    }
}
