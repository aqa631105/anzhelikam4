package utils;

import java.util.Random;

/**
 * This is  public class {@EmailGenerator} for https://petstore.swagger.io/
 * Utility class for generating random email addresses.
 * {@Author} Anzhelika
 * Created by Anzhelika.Miroshnichenko
 * Date 01.06.2024
 */
public class EmailGenerator {
    private static final String CHARACTERS = "abcdefghijklmnopqrstuvwxyz0123456789";
    private static final String DOMAIN = "test.com";

    /**
     * Generates a random email address.
     *
     * @return A randomly generated email address in the format "randomString@test.com".
     */
    public static String emailGenerator() {
        Random random = new Random();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 15; i++) {
            int index = random.nextInt(CHARACTERS.length());
            char randomChar = CHARACTERS.charAt(index);
            stringBuilder.append(randomChar);
        }

        return stringBuilder + "@" + DOMAIN;
    }
}
