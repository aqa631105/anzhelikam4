package hellocucumber;

import entityenum.UserEntity;
import entityfactory.UserFactory;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * This is  public class {@CrudTestApi} for https://petstore.swagger.io/
 * Class CrudTestApi contains tests for https://petstore.swagger.io/
 * {@Author} Anzhelika
 * Created by Anzhelika.Miroshnichenko
 * Date 01.06.2024
 */
public class CrudStepDefinition {

    public Response response;

    @Before
    public void setUp() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("src/test/resources/test.properties"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        RestAssured.baseURI = properties.getProperty("baseApiUri");
    }

    @When("I create a new user")
    @Given("User with required data exists")
    public void iCreateANewUser() {
        response = given()
                .header("Content-Type", "application/json")
                .body(UserFactory.createNewUser())
                .when()
                .post("/user");
    }

    @Then("Status code is {int}")
    public void statusCodeIs(int statusCode) {
        assertEquals(statusCode, response.getStatusCode());
    }

    @When("I get the user")
    public void iGetTheUser() {
        response = given()
                .header("Content-Type", "application/json")
                .log().all()
                .get("/user/" + UserEntity.USERNAME.getFieldName());
    }

    @And("Response contains user's username")
    public void responseContainsUserSUsername() {
        response.then().body("username", equalTo(UserEntity.USERNAME.getFieldName()));
    }

    @When("I update the user with {string}")
    public void iUpdateTheUserWithEmail(String newEmail) {
        response = given()
                .header("Content-Type", "application/json")
                .body(UserFactory.updateUserEmail(newEmail))
                .when()
                .put("/user/" + UserEntity.USERNAME.getFieldName());
    }

    @When("I delete the user")
    public void iDeleteTheUser() {
        given()
                .header("Content-Type", "application/json")
                .log().all()
                .delete("/user/" + UserEntity.UPDATED_USERNAME.getFieldName());
    }

    @And("Impossible to get deleted user")
    public void impossibleToGetDeletedUser() {
        given()
                .header("Content-Type", "application/json")
                .log().all()
                .get("/user/" + UserEntity.UPDATED_USERNAME.getFieldName())
                .then()
                .log().all()
                .statusCode(404)
                .body("message", equalTo("User not found"));
    }
}
