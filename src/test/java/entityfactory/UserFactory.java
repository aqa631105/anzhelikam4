package entityfactory;

import entity.User;
import entityenum.UserEntity;

/**
 * This is  public class {@UserFactory} for https://petstore.swagger.io/
 * Factory class for creating and updating user objects.
 * {@Author} Anzhelika
 * Created by Anzhelika.Miroshnichenko
 * Date 01.06.2024
 */
public class UserFactory {

    /**
     * Creates a new user object based on predefined user entity fields.
     *
     * @return A new User object with predefined field values.
     */
    public static User createNewUser() {
        return new User(UserEntity.USERNAME.getFieldName(),
                UserEntity.FIRST_NAME.getFieldName(),
                UserEntity.LAST_NAME.getFieldName(),
                UserEntity.EMAIL.getFieldName(),
                UserEntity.PASSWORD.getFieldName(),
                UserEntity.PHONE.getFieldName(),
                UserEntity.USERSTATUS.getFieldInt(),
                UserEntity.USERID.getFieldInt());
    }

    /**
     * Updates an existing user object with predefined updated username field.
     *
     * @return An updated User object with the updated username field value.
     */

    public static User updateUserEmail(String newEmail) {
        return new User(
                UserEntity.UPDATED_USERNAME.getFieldName(),
                UserEntity.FIRST_NAME.getFieldName(),
                UserEntity.LAST_NAME.getFieldName(),
                newEmail,
                UserEntity.PASSWORD.getFieldName(),
                UserEntity.PHONE.getFieldName(),
                UserEntity.USERSTATUS.getFieldInt(),
                UserEntity.USERID.getFieldInt()
        );
    }
}
