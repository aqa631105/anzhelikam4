package entity;

/**
 * Record representing a user entity with various properties.
 */
public record User(String username, String firstName, String lastName, String email, String password, String phone,
                   int userStatus, int id) {
}